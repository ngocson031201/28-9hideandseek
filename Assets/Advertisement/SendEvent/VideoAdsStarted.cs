using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SendEventAppMetrica
{
    public class VideoAdsStarted
    {
        private const string nameEvent = "video_ads_started";
        public static EventAppmetrica eventAppmetrica;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ad_type">interstitial, rewarded</param>
        /// <param name="placement">ad_on_replay, get_health, get_shop_entry</param>
        /// <param name="result">success, not_available etc.</param>
        /// <param name="connection">1, 0 (true, false)</param>
        public static void SetEvent(string ad_type,  string placement, string result, bool connection)
        {
            eventAppmetrica.parameters[VideoAdsStartedParameters.ad_type] = ad_type;
            eventAppmetrica.parameters[VideoAdsStartedParameters.placement] = placement;
            eventAppmetrica.parameters[VideoAdsStartedParameters.result] = result;
            eventAppmetrica.parameters[VideoAdsStartedParameters.connection] = connection;
        }

        /// <summary>
        /// Thay đổi đơn lẻ giá trị levelstart
        /// </summary>
        /// <param name="nameParameter">Get from VideoAdsStartedParameters.</param>
        /// <param name="value">eg: bool, int, string</param>
        public static void SetEvent(string nameParameter, object value)
        {
            eventAppmetrica.parameters[nameParameter] = value;
        }

        public static void InitDefault()
        {
            eventAppmetrica = new EventAppmetrica(nameEvent, new Dictionary<string, object>()
            {
                {VideoAdsStartedParameters.ad_type, "init"},
                {VideoAdsStartedParameters.placement, "init"},
                {VideoAdsStartedParameters.result, "init"},
                {VideoAdsStartedParameters.connection, false},
            });
        }
        
        public static void CustomParameters(Dictionary<string, object> parameter)
        {
            eventAppmetrica = new EventAppmetrica(nameEvent, parameter);
        }
        
        public static EventAppmetrica GetEventAppmetrica()
        {
            return eventAppmetrica;
        }
    }
    
    [System.Serializable]
    public static class VideoAdsStartedParameters
    {
        public const string ad_type = "ad_type";
        public const string placement = "placement";
        public const string result = "result";
        public const string connection = "connection";
    }
}

