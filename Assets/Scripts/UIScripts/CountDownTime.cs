﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
public class CountDownTime : MonoBehaviour
{
    public UnityEvent AndTimeWin,AndTimeLose;
    public static CountDownTime instance;
    [SerializeField] Image SilderTime;
    [SerializeField] Text TimeText;
    public float Duration, CurrentTime;
    public bool isEndGame =false;

    public float DurationStartime, currentStartTime;
    [SerializeField] Text TimeStartText;
    [SerializeField] GameObject SilderTimePanel;
    [SerializeField] GameObject TimeStartingCounDown;
    [SerializeField] Text CountDownText01;
    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        currentStartTime = DurationStartime;
        TimeStartText.text = currentStartTime.ToString();
        StartCoroutine(TimeStart());
        CurrentTime = Duration;

    }

    IEnumerator TimeStart()
    {
        AudioInsideGame.instance.SoundIngame();
        while (currentStartTime >= 0)
        {
            TimeStartText.text = currentStartTime.ToString();
            yield return new WaitForSeconds(1f);
            currentStartTime--;
        }
        TimeStartingCounDown.SetActive(false);
        SilderTimePanel.SetActive(true);

        TimeText.text = CurrentTime.ToString();
        StartCoroutine(TimeEnd());
    }


    IEnumerator TimeEnd()
    {
        if(GameManager.instance.level == 4|| GameManager.instance.level==9||GameManager.instance.level==14|| GameManager.instance.level == 19 || GameManager.instance.level == 24 || GameManager.instance.level == 29|| GameManager.instance.level == 34 || GameManager.instance.level == 39 || GameManager.instance.level == 44|| GameManager.instance.level == 49)
        {
            CurrentTime = 15f;

        }
        else
        {
            CurrentTime = 30f;
        }    
        while (CurrentTime>=0)
        {
            

            SilderTime.fillAmount = Mathf.InverseLerp(0, Duration, CurrentTime);
            
            TimeText.text = CurrentTime.ToString();
            yield return new WaitForSeconds(1f);
            CurrentTime--;


            if (CurrentTime <= 19 && CurrentTime > 9)
            {
                TimeText.color = Color.yellow;
                CountDownText01.color = Color.yellow;
                SilderTime.color = Color.yellow;
            }
            else if (CurrentTime <= 9)
            {
                TimeText.color = Color.red;
                CountDownText01.color = Color.red;
                SilderTime.color = Color.red;
            }
            else if (CurrentTime > 19)
            {
                TimeText.color = Color.green;
                CountDownText01.color = Color.green;
                SilderTime.color = Color.green;
            } 
            
            //TH1: Tìm được cả 5 Hide -->Win
            if(PatrolAgent.HideArrested ==5)
            {

                //if(Prefs.FULLADS==0)
                //{
                //    ManagerAds.ins.ShowInterstitial();
                //}    

                AndTimeWin?.Invoke();
            }    
        }


        AudioInsideGame.instance.StopSoundInside();
        if (GameManager.instance.level == 4 || GameManager.instance.level == 9 || GameManager.instance.level == 14 || GameManager.instance.level == 19 || GameManager.instance.level == 24 || GameManager.instance.level == 29 || GameManager.instance.level == 34 || GameManager.instance.level == 39 || GameManager.instance.level == 44 || GameManager.instance.level == 49)
        {

            PatrolAgent.HideArrested = 0;
            //if(SpawnManager.CheckHide00 == true)
            //{
            //    PlayerController.instance.Sensor.SetActive(false);
            //}
            
                
            //if (Prefs.FULLADS == 0)
            //{
            //    ManagerAds.ins.ShowInterstitial();
            //}
            AndTimeWin?.Invoke();
            
        }
        else
        {

            if (PatrolAgent.HideArrested >= 3)
            {

                //if (Prefs.FULLADS == 0)
                //{
                //    ManagerAds.ins.ShowInterstitial();
                //}
                AndTimeWin?.Invoke();
            }
            else 
            {
                AndTimeLose?.Invoke();
            }
        }

    }
}
