using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenBox : MonoBehaviour
{
    public GameObject BoxHide;
    public GameObject[] ShowBox;
    public static int index;

    private void Start()
    {
        BoxHide.SetActive(true);
    }

    public void ShowBoxx()
    {
        if (Prefs.KEY >= 3)
        {
            BoxHide.SetActive(false);
            int index = Random.Range(0, ShowBox.Length);
            ShowBox[index].SetActive(true);

            for (int i = 0; i < ShowBox.Length; i++)
            {
                if (i != index)
                {
                    ShowBox[i].SetActive(false);
                }
            }

            if (index == 1)
            {
                ScoreManager.instance.AddCoinDaily(50);
            }

            if (index == 2)
            {
                ScoreManager.instance.AddCoinDaily(100);
            }

            if (index == 3 || index == 7)
            {
                ScoreManager.instance.AddCoinDaily(150);
            }

            if (index == 4)
            {
                ScoreManager.instance.AddCoinDaily(200);
            }

            if (index == 5)
            {
                ProfileShopUI.instance.addImageChest();
            }
            Prefs.KEY = 0;
        }
    }
}
