using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footprint : MonoBehaviour
{
    [Tooltip("this is how long the decal will stay, before it shrinks away totally")]
    [SerializeField] float Lifetime = 100f;

    private float mark;
    private Vector3 OrigSize;
    public void Start()
    {
        mark = Time.time;
        OrigSize = this.transform.localScale;
    }

    public void Update()
    {
       // ChangeColorFoot();
        float ElapsedTime = Time.time- mark;
        if (ElapsedTime != 0)
        {
            float PercentTimeLeft = (5 - ElapsedTime) / 5;

            this.transform.localScale = new Vector3(OrigSize.x * PercentTimeLeft, OrigSize.y * PercentTimeLeft, OrigSize.z * PercentTimeLeft);
            if (ElapsedTime > 5)
            {
                Destroy(this.gameObject);
            }
        }
    }
}
