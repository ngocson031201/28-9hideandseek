﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Cinemachine;
[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{
    public static PlayerController instance;
    public NavMeshAgent agent;
    [SerializeField] private Rigidbody _rg;
    [SerializeField] private FloatingJoystick _joystick;
    public float _moveSpeed;
    Animator animator;

    public Transform GroundCheck;
    public Transform FloorCheck;
    public LayerMask groundSwimMask;
    public LayerMask FloorMask;
    [SerializeField] float GroundCheckDistance = 0.4f;

    public bool isMove;
    bool isGrounded;
    bool isFloor;
    public Camera mainCamera;
    [SerializeField] GameObject CanvasJoyStick;
    public GameObject Sensor;
    private float currentMoveSpeed;

    [SerializeField] GameObject[] skins;

    [SerializeField] ParticleSystem EffectWalkThroughWall;
    bool checkBulletWalkThroughWall;
    public float timeCountDownThroughWall;

    [SerializeField] ParticleSystem EffectInvisibility;
    public float timeCountDownInvisibility;
    public bool checkBulletInvisibility;

    [SerializeField] ParticleSystem EffectXray;
    public float timeCountDownXray;
    public static bool checkBulletXray;
    public float StartSizeXrayFX;
    public ParticleSystem.MainModule Pmain;

    public Transform transformParent;


    private void Awake()
    {
        instance = this;
        if (mainCamera == null)
            mainCamera = Camera.main;

    }
    private void Start()
    {
        currentMoveSpeed = 3f;
        animator = GetComponentInChildren<Animator>();
        Pmain =EffectXray.main;
    }

    void Update()
    {
        changeSkins();

        if (UIController.instance.isPlayGame == true)
        {
            if (SpawnManager.ChangeStatic == true)
            {

                StartCoroutine(TimeCountDownPlayGamePlayerHide());
                Debug.Log("ChangeStatic00" + SpawnManager.ChangeStatic);
            }
            else
            {
                StartCoroutine(TimeCountDownPlayGame());
                Debug.Log("ChangeStatic01" + SpawnManager.ChangeStatic);
            }
        }
        //chạm vào để hiển thị nút di chuyển
        Touch();

        CheckGroundToSwim();

        //kiểm tra thử về anim mở cử lồng nhốt
        prisonAnim();

        if (UIController.instance.isWin == true)
        {
            WinAnim();
            DisableJoystick();
        }

        if (UIController.instance.isLose == true)
        {
            loseAnim();
        }

        CheckFloor();
        CheckDisableJoyStick(); 
    }

    public void CheckDisableJoyStick()
    {
        if (UIController.instance.isLose == true)
            DisableJoystick();

    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "PaintsGlue")
        {

            StartCoroutine(changeMoveSpeedSlow());

        }

        if (other.gameObject.tag == "Speed")
        {

            StartCoroutine(changeMoveSpeedFast());
            Destroy(other.gameObject);

        }

        if (other.gameObject.tag == "Clock")
        {
            CountDownTime.instance.CurrentTime += 10f;
            Destroy(other.gameObject);

        }

        if (other.gameObject.tag == "BulletThroughWalls")
        {
            Debug.Log("BulletThroughWalls");
            StartCoroutine(TimeBreakThroughWall());
            AudioController.instance.BulletSound();
            Destroy(other.gameObject);
        }

        if (other.gameObject.tag == "BulletInvisibility")
        {
            Debug.Log("BulletInvisibility");
            StartCoroutine(TimeBreakInvisibility());
            AudioController.instance.BulletSound();
            Destroy(other.gameObject);
        }

        if (other.gameObject.tag == "BulletXray")
        {
            if(CountDownTime.instance.CurrentTime>10)
            {
                StartCoroutine(TimeBreakXray());
                AudioController.instance.BulletSound();
                Destroy(other.gameObject);
            }

        }

    }

    IEnumerator TimeBreakXray()
    {
        EffectXray.Play();
        checkBulletXray = true;
        PatrolAgent.instance.ShowCharactorAI(true);
        Pmain.startSize = new ParticleSystem.MinMaxCurve(12f);
        yield return new WaitForSeconds(9f);
        checkBulletXray = false;
        PatrolAgent.instance.ShowCharactorAI(false);
        Pmain.startSize = new ParticleSystem.MinMaxCurve(0f);
        EffectXray.Stop();
        

    }

    
    IEnumerator TimeBreakInvisibility()
    {
        timeCountDownInvisibility = 12f;
        while (timeCountDownInvisibility > 0)
        {
            checkBulletInvisibility = true;
            if (timeCountDownInvisibility > 2)
            {
                EffectInvisibility.Play();
            }
            else
            {
                EffectInvisibility.Stop();
            }

            yield return new WaitForSeconds(1f);
            timeCountDownInvisibility--;
            Debug.Log(timeCountDownInvisibility);

        }
        checkBulletInvisibility = false;


    }



    IEnumerator TimeBreakThroughWall()
    {
        timeCountDownThroughWall = 10f;
        checkBulletWalkThroughWall = true;
        EffectWalkThroughWall.Play();
        agent.enabled = false;
        yield return new WaitForSeconds(timeCountDownThroughWall);
        agent.enabled = true;
        EffectWalkThroughWall.Stop();
        checkBulletWalkThroughWall = false;
    }

    public void CheckFloor()
    {
        isFloor = Physics.CheckSphere(FloorCheck.position, 0.1f, FloorMask);
        if (isFloor && checkBulletWalkThroughWall == true)
        {
            Debug.Log("CheckTrue");
            agent.enabled = true;
        }

        if (!isFloor && timeCountDownThroughWall > 0)
        {
            agent.enabled = false;
        }

        if (checkBulletWalkThroughWall == false)
        {
            agent.enabled = true;
        }
    }



    private void changeSkins()
    {
        int selectedIndex = ProfileShopUI.IDIndexSkin - 1;
        skins[selectedIndex].SetActive(true);
        for (int i = 0; i < skins.Length; i++)
        {
            if (i != selectedIndex)
            {
                skins[i].SetActive(false);
            }
        }
    }

    IEnumerator changeMoveSpeedSlow()
    {
        _moveSpeed = _moveSpeed / 2 + 0.2f;
        yield return new WaitForSeconds(5f);
        _moveSpeed = currentMoveSpeed;
    }


    IEnumerator changeMoveSpeedFast()
    {
        _moveSpeed = _moveSpeed + 0.75f;
        yield return new WaitForSeconds(5f);
        _moveSpeed = currentMoveSpeed;
    }

    public void Touch()
    {

        if (Input.GetMouseButton(0))
        {
            isMove = true;
            Vector3 mousePos = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            PlayerMove();
            animator.SetFloat("MoveAnim", 1);
        }
        else
        {
            _rg.velocity = new Vector3(_joystick.Horizontal, _rg.velocity.y, _joystick.Vertical);
            animator.SetFloat("MoveAnim", 0);
            animator.SetFloat("IdleAnim", 1);
            isMove = false;
        }

    }

    public void PlayerMove()
    {
        // di chuyển nhân vật
        _rg.velocity = new Vector3(_joystick.Horizontal * _moveSpeed, _rg.velocity.y, _joystick.Vertical * _moveSpeed);

        //Kiểm tra xem nhân vật có di chuyển hay không?
        if (_joystick.Horizontal != 0 || _joystick.Vertical != 0)
        {
            //code khả năng xoay người của nhân vật
            transform.rotation = Quaternion.LookRotation(_rg.velocity);

        }
    }

    public void CheckGroundToSwim()
    {
        isGrounded = Physics.CheckSphere(GroundCheck.position, GroundCheckDistance, groundSwimMask);
        if (isGrounded)
        {
            animator.SetBool("isGroundSwim", true);
        }
        else
        {
            animator.SetBool("isGroundSwim", false);

        }

        if (isGrounded && isMove == false)
        {
            animator.SetBool("isGroundSwimIdle", true);
        }

        if (isGrounded && isMove == true)
        {
            animator.SetBool("isGroundSwimIdle", false);
        }
    }

    public void prisonAnim()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            animator.SetBool("isPrison", true);
        }
        else
        {
            animator.SetBool("isPrison", false);
        }
    }


    public void WinAnim()
    {

        animator.SetBool("isWin", true);

    }

    public void loseAnim()
    {
        animator.SetBool("isClose", true);
    }

    public IEnumerator TimeCountDownPlayGame()
    {
        yield return new WaitForSeconds(3f);

        if (CountDownTime.instance.CurrentTime > 0)
        {
            CanvasJoyStick.SetActive(true);
            if (GameManager.instance.level == 4 || GameManager.instance.level == 9 || GameManager.instance.level == 14 || GameManager.instance.level == 19 || GameManager.instance.level == 24 || GameManager.instance.level == 29 || GameManager.instance.level == 34 || GameManager.instance.level == 39 || GameManager.instance.level == 44 || GameManager.instance.level == 49)
            {
                if (SpawnManager.instance.CheckHide00 == true)
                {

                    Sensor.SetActive(false);
                }
                else
                {
                    Sensor.SetActive(true);
                }

            }
            else
            {
                Sensor.SetActive(true);
            }

        }
        else
        {

            DisableJoystick();
        }
    }

    public void DisableJoystick()
    {
        CanvasJoyStick.SetActive(false);
        isMove = false;
        _rg.isKinematic = true;
    }

    IEnumerator TimeCountDownPlayGamePlayerHide()
    {
        yield return new WaitForSeconds(0.001f);

        if (CountDownTime.instance.CurrentTime > 0)
        {
            CanvasJoyStick.SetActive(true);

            if (GameManager.instance.level == 4 || GameManager.instance.level == 9 || GameManager.instance.level == 14 || GameManager.instance.level == 19 || GameManager.instance.level == 24 || GameManager.instance.level == 29 || GameManager.instance.level == 34 || GameManager.instance.level == 39 || GameManager.instance.level == 44 || GameManager.instance.level == 49)
            {
                if(SpawnManager.instance.CheckHide00 == true)
                {

                    Sensor.SetActive(false);
                }
                else
                {
                    Sensor.SetActive(true);
                }
                    
            }
            else
            {
                Sensor.SetActive(true);
            }

        }
        else
        {
            //UIController.instance.isWin = true;
            UIController.instance.EndTimeWin();

        }
    }
}
