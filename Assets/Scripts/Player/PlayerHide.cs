﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerHide : MonoBehaviour
{
    [SerializeField] GameObject cagePlayerHide;
    Rigidbody rg;
    [SerializeField] ParticleSystem EffectWalkThroughWall;
    public NavMeshAgent agent;
    PlayerController PlayerController;
    private void Start()
    {
        cagePlayerHide.SetActive(false);
        rg = GetComponent<Rigidbody>();
        PlayerController = GetComponent<PlayerController>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "sensor" && PlayerController.checkBulletInvisibility==false)
        {
            cagePlayerHide.SetActive(true);
            PlayerController.instance.DisableJoystick();
            PlayerController.instance.loseAnim();
            UIController.instance.EndTimeLose();
            
        }

        //if (other.gameObject.tag == "CheckPlayerHide")
        //{
        //    PlayerController.instance.DisableJoystick();
        //    PlayerController.instance.loseAnim();
        //    UIController.instance.EndTimeLose();
        //}

    }

}
