﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;
public class PatrolAgentAI : MonoBehaviour
{
    public static PatrolAgentAI instance;
    [SerializeField] private Transform[] points;
    [SerializeField] float RenmainingDistance = 2f;

    public float waitTime = 1f;
    float time = 0;
    public Transform GroundCheck;
    [SerializeField] float GroundCheckDistance = 0.4f;
    public LayerMask groundSwimMask;

    bool isGrounded;

    Animator animator;
    bool isMove;

    int destinationPoint = 0;
    public NavMeshAgent agent;

    private float currentSpeedAgent;
    public Transform transformParent;

    [SerializeField] GameObject Sensor;

    public BoxCollider box;
    bool isWall;
    bool isPlayerHide;
    public Transform WallCheck;
    public LayerMask WallMask;
    public LayerMask PlayerHideMask;
    Vector3 originalSize = new Vector3(1.1f, 0.3f, 2.8f);
    Vector3 changedSize = new Vector3(0, 0, 0);
    private void Awake()
    {
        box.size = originalSize;
        instance = this;
    }

    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.autoBraking = false;
        currentSpeedAgent = agent.speed;
        animator = GetComponentInChildren<Animator>();
        if (UIController.instance.isPlayGame == true)
        {
            moveToFirstPoint();
        }
    }

    public void ScaleAISeek()
    {
        switch (PatrolAgent.HideArrested)
        {
            case 1:
                transformParent.DOScale(new Vector3(1.1f, 1.1f, 1.1f), 15f).SetEase(Ease.OutBack);
                break;
            case 2:
                transformParent.DOScale(new Vector3(1.2f, 1.2f, 1.2f), 15f).SetEase(Ease.OutBack);
                break;
            case 3:
                transformParent.DOScale(new Vector3(1.3f, 1.3f, 1.3f), 15f).SetEase(Ease.OutBack);
                break;
            case 4:
                transformParent.DOScale(new Vector3(1.4f, 1.4f, 1.4f), 15f).SetEase(Ease.OutBack);
                break;
            case 5:
                transformParent.DOScale(new Vector3(1.5f, 1.5f, 1.5f), 15f).SetEase(Ease.OutBack);
                break;
        }
    }    

    private void Update()
    {
        //CheckPlayerHide();
        //CheckWall();
        ScaleAISeek();

        if (UIController.instance.isPlayGame == true)
        {
            StartCoroutine(TimeCountDownPlayGame());
        }

        if (UIController.instance.isPlayGame == false)
        {
            agent.speed = 0f;
            animator.SetFloat("MoveAnim", 0);
            animator.SetFloat("IdleAnim", 1);
            isMove = false;
        }


        if (!agent.pathPending && agent.remainingDistance < RenmainingDistance && UIController.instance.isPlayGame == true)
        {

            GoToNextPoint();
        }
        CheckGroundToSwim();
    }


    //public void CheckWall()
    //{
    //    isWall = Physics.CheckSphere(WallCheck.position, 1f, WallMask);
    //    {
    //        if(isWall)
    //        {
    //            box.size = changedSize;
    //        }  

    //    }
    //}

    //public void CheckPlayerHide()
    //{
    //    isPlayerHide = Physics.CheckSphere(WallCheck.position, 1f, PlayerHideMask);
    //    {
    //        if (isPlayerHide)
    //        {
    //            box.size = originalSize;
    //        }
    //    }
    //}

    IEnumerator TimeCountDownPlayGame()
    {
        agent.speed = 0;
        animator.SetFloat("MoveAnim", 0);
        animator.SetFloat("IdleAnim", 1);
        yield return new WaitForSeconds(3f);
        agent.speed = 2.5f;
        animator.SetFloat("MoveAnim", 1);
        animator.SetFloat("IdleAnim", 0);
        if (CountDownTime.instance.CurrentTime > 0)
        {
            Sensor.SetActive(true);

        }
        else
        {
            isMove = false;
        }    

    }
    public void moveToFirstPoint()
    {

        int random = Random.Range(0, points.Length);
        agent.destination = points[random].position;
        animator.SetFloat("MoveAnim", 1);
    }
    public void GoToNextPoint()
    {

        if (points.Length == 0)
        {
            Debug.Log(" bạn cần setup thêm point");
            enabled = false;
            return;
        }

        time += Time.deltaTime;
        if (time > waitTime)
        {
            agent.speed = currentSpeedAgent;
            animator.SetFloat("IdleAnim", 0);
            animator.SetFloat("MoveAnim", 1);
            agent.destination = points[destinationPoint].position;
            destinationPoint = (destinationPoint + 1) % points.Length;

            isMove = true;
            time = 0;

        }
        else
        {
            agent.speed = 0;
            animator.SetFloat("MoveAnim", 0);
            animator.SetFloat("IdleAnim", 1);
            isMove = false;

        }

    }

    public void CheckGroundToSwim()
    {
        isGrounded = Physics.CheckSphere(GroundCheck.position, GroundCheckDistance, groundSwimMask);
        if (isGrounded)
        {
            animator.SetBool("isGroundSwim", true);
        }
        else
        {
            animator.SetBool("isGroundSwim", false);
        }

        if (isGrounded && isMove == false)
        {
            animator.SetBool("isGroundSwimIdle", true);
        }

        if (isGrounded && isMove == true)
        {
            animator.SetBool("isGroundSwimIdle", false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        bool Checked = Physics.CheckSphere(GroundCheck.position, GroundCheckDistance, groundSwimMask);

        if (other.gameObject.tag == "PaintsGlue")
        {
            StartCoroutine(changeMoveSpeedSlow());
        }

    }
    IEnumerator changeMoveSpeedSlow()
    {
        agent.speed = agent.speed / 2.5f;
        yield return new WaitForSeconds(5f);
        agent.speed = currentSpeedAgent;
    }
}
