using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DataPlayer
{
    private const string ALL_DATA = "all_data";
    private static ALLDATA alldata;

    static DataPlayer()
    {
        alldata = JsonUtility.FromJson<ALLDATA>(PlayerPrefs.GetString(ALL_DATA));
        if(alldata == null)
        {
            var skinDefault = 1;
            alldata = new ALLDATA
            {
                shopItem = new List<int>(skinDefault),
            };
            SaveData();
        }
    }

    public static void SaveData()
    {
        var data = JsonUtility.ToJson(alldata);
        PlayerPrefs.SetString(ALL_DATA,data);
    }

    public static bool isOwned(int id)
    {
        return alldata.isOwned(id);
    }

    public static void AddSkin(int id)
    {
        alldata.AddSkin(id);
        SaveData();
    }
}

public class ALLDATA
{
    public List<int> shopItem;

    public bool isOwned(int id)
    {
        return shopItem.Contains(id);
    }

    public void AddSkin(int id)
    {
        if (isOwned(id)) return;

        shopItem.Add(id);
    }
}
