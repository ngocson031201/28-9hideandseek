using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class Coin : MonoBehaviour
{
    [SerializeField] float turnSpeed = 90f;


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Wall" || other.gameObject.tag == "Floor"|| other.gameObject.tag == "Gold" || other.gameObject.tag == "Diamond")
        {
            Destroy(gameObject);
            return;
        }

        if (other.gameObject.GetComponent<PlayerController>() != null|| other.gameObject.tag=="PlayerHide")
        {
            AudioController.instance.CoinMusic();
            ScoreManager.instance.AddPoint();
            UIController.instance.CoinTextt(true);

            Destroy(gameObject);
            return;
        }

        // Check that the object we collided with is the player
        if (other.gameObject.name != "Player")
        {
            return;
        }

        Destroy(gameObject);
    }

  
    IEnumerator TimeBreak()
    {
        yield return new WaitForSeconds(0.3f);

    }

    private void Update()
    {
        transform.Rotate(turnSpeed * Time.deltaTime, 0, 0);
    }

}
