using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class FootPaint : MonoBehaviour
{
    #region --- helpers --
    public enum enumFoot
    {
        Left,
        Right,
    }
    #endregion

    public GameObject LeftPrefab;
    public GameObject RightPrefab;
    public float FootprintSpacer = 1.0f;
    private Vector3 LastFootprint;
    private enumFoot WhichFoot;
    public GameObject[] intdexPos;
    public GameObject ThisObject;
    #region boolPaint
    [HideInInspector]
    public bool isPaint = false;
    [HideInInspector]
    public bool isPaintYellow = false;
    [HideInInspector]
    public bool isPaintRed = false;
    [HideInInspector]
    public bool isPaintBlue = false;
    [HideInInspector]
    public bool isPaintGreen = false;

    public Transform dir;
    #endregion
    private void Start()
    {
        Color color = Color.clear;
        LastFootprint = this.transform.position;
        LeftPrefab.GetComponent<SpriteRenderer>().color = color;
        RightPrefab.GetComponent<SpriteRenderer>().color = color;
    }

    private void Update()
    {

        FootPaints();
        CheckPaint();

    }

    public void FootPaints()
    {
        if (GetComponent<CheckPaintItem>().isPaint ==true)
        {
            float DistanceSinceLastFootprint = Vector3.Distance(transform.position, LastFootprint);
            if (DistanceSinceLastFootprint >= FootprintSpacer)
            {
                LastFootprint = this.transform.position;
                if (WhichFoot == enumFoot.Left)
                {
                    SpawnFootDecal(LeftPrefab);
                    WhichFoot = enumFoot.Right;
                }
                else if (WhichFoot == enumFoot.Right)
                {
                    SpawnFootDecal(RightPrefab);
                    WhichFoot = enumFoot.Left;
                }
                LastFootprint = new Vector3(this.transform.position.x, this.transform.position.y + 1f, this.transform.position.z);
            }
        }

    }

    public void CheckPaint()
    {
        if(GetComponent<CheckPaintItem>().isPaintYellow == true && GetComponent<CheckPaintItem>().isPaint == true)
        {
            LeftPrefab.GetComponent<SpriteRenderer>().color = Color.yellow;
            RightPrefab.GetComponent<SpriteRenderer>().color = Color.yellow;
        }

        if (GetComponent<CheckPaintItem>().isPaintRed == true && GetComponent<CheckPaintItem>().isPaint == true)
        {
            LeftPrefab.GetComponent<SpriteRenderer>().color = Color.red;
            RightPrefab.GetComponent<SpriteRenderer>().color = Color.red;
        }

        if (GetComponent<CheckPaintItem>().isPaintBlue == true && GetComponent<CheckPaintItem>().isPaint == true)
        {
            LeftPrefab.GetComponent<SpriteRenderer>().color = Color.blue;
            RightPrefab.GetComponent<SpriteRenderer>().color = Color.blue;
        }

        if (GetComponent<CheckPaintItem>().isPaintGreen == true && GetComponent<CheckPaintItem>().isPaint == true)
        {
            LeftPrefab.GetComponent<SpriteRenderer>().color = Color.green;
            RightPrefab.GetComponent<SpriteRenderer>().color = Color.green;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Color color = Color.clear;
        LeftPrefab.GetComponent<SpriteRenderer>().color = color;
        RightPrefab.GetComponent<SpriteRenderer>().color = color;
    }


    private void OnTriggerStay(Collider other)
    {

        if (other.gameObject.tag == "PaintsYellow")
        {
            Debug.Log("Yellow");
            isPaint = true;
            isPaintYellow = true;
            StartCoroutine(timePaint());
        }
        else if (other.gameObject.tag == "PaintsRed")
        {
            isPaint = true;
            isPaintRed = true;
            StartCoroutine(timePaint());
        }
        else if (other.gameObject.tag == "PaintsBlue")
        {
            isPaint = true;
            isPaintBlue = true;
            StartCoroutine(timePaint());
        }
        else if (other.gameObject.tag == "PaintsGreen")
        {
            isPaint = true;
            isPaintGreen = true;
            StartCoroutine(timePaint());
        }
        else
        {
            isPaint = false;
            isPaintYellow = false;
            isPaintRed = false;
            isPaintBlue = false;
            isPaintGreen = false;
        }
    }



    IEnumerator timePaint()
    {
        yield return new WaitForSeconds(10f);
        isPaint = false;

    }

    public void SpawnFootDecal(GameObject prefab)
    {
        GameObject decal = Instantiate(prefab);

        decal.transform.position = intdexPos[0].transform.position;


        GameObject decal1 = Instantiate(prefab);
        decal1.transform.position = intdexPos[1].transform.position;

        Quaternion rotate = Quaternion.LookRotation(dir.position - transform.position);
        decal.transform.rotation = rotate;
        decal1.transform.rotation = rotate;

    }
}
