﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public static SpawnManager instance;

    #region parameter Spawn
    [SerializeField] string SpawnPointAITag = "SpawnPointAI";
    public List<GameObject> PrefabsAI;

    [SerializeField] string SpawnPointBulletTag = "SpawnBullet";
    public List<GameObject> PrefabsBullet;

    [SerializeField] string SpawnPointPlayerTag ="SpawnPointPlayer";
    [SerializeField] GameObject PrefabPlayer;

    [SerializeField] string SpawnPointBulletInvisibilityTag = "BulletInvisibilityPoint";
    [SerializeField] GameObject PrefabBulletInvisibility;

    [SerializeField] string SpawnPointBulletXrayTag = "BulletXrayPoint";
    [SerializeField] GameObject PrefabBulletXray;

    [SerializeField] GameObject PrefabPlayerHide;

    [SerializeField] string SpawnPointPlayer01Tag = "SpawnPointPlayer01";
    [SerializeField] GameObject PrefabPlayer01;

    [SerializeField] string SpawnPointPaintsTag ="SpawnPointPaint";
    [SerializeField] List<GameObject> PrefabsPaints;

    [SerializeField] string SpawnPointSpeedTag ="SpawnPointSpeed";
    [SerializeField] List<GameObject> PrefabSpeed;

    [SerializeField] string SpawnPointClockTag ="SpawnPointClock";
    [SerializeField] List<GameObject> PrefabsClock;

    #endregion

    #region SpawnBG
    public GameObject[] MapBackGround;
    #endregion


    [SerializeField] bool alwaySpawn = true;
    public int count =0;
    GameObject[] AIPoint;

    GameObject Player01;
    public static bool ChangeStatic;
    public bool CheckHide00;
    public GameObject ChangeBossSeekGreen00;
    public GameObject ChangeBossSeekRed00;
    public GameObject ChangeBossSeekGreen01;
    public GameObject ChangeBossSeekRed01;
    private void Awake()
    {
        instance = this;
        SpawnMapBG();
        
    }
    private void Start()
    {
        ChangeStatic = false;
        AIPoint = GameObject.FindGameObjectsWithTag(SpawnPointAITag);
        SpawnPlayerCenter();
        SpawnAIAround();
        SpawnPaints();
        SpawnClock();
        SpawnSpeed();
        //ChangeStatic = false;
    }

    public void SpawnMapBG()
    {
        int index = Random.Range(0, MapBackGround.Length);
        GameObject pts = Instantiate(MapBackGround[index]);
        pts.transform.position = new Vector3(8f, -42f, -60f);
        

    }    

    public void CheckMap()
    {
        if (GameManager.instance.level == 4 || GameManager.instance.level == 9 || GameManager.instance.level == 14 || GameManager.instance.level == 19 || GameManager.instance.level == 24 || GameManager.instance.level == 29 || GameManager.instance.level == 34 || GameManager.instance.level == 39 || GameManager.instance.level == 44 || GameManager.instance.level == 49)
        {
            return;

        }
    }    
    public void SpawnBullets()
    {
        if (GameManager.instance.level == 4 || GameManager.instance.level == 9 || GameManager.instance.level == 14 || GameManager.instance.level == 19 || GameManager.instance.level == 24 || GameManager.instance.level == 29 || GameManager.instance.level == 34 || GameManager.instance.level == 39 || GameManager.instance.level == 44 || GameManager.instance.level == 49)
        {
            return;
        }

        GameObject[] BulletPoint = GameObject.FindGameObjectsWithTag(SpawnPointBulletTag);

        foreach (GameObject item in BulletPoint)
        {
            int random = Random.Range(0, PrefabsBullet.Count);
            if (alwaySpawn)
            {
                GameObject pts = Instantiate(PrefabsBullet[random]);
                pts.transform.position = item.transform.position;
                pts.transform.rotation = item.transform.rotation;
            }
        }
    }
    public void SpawnPlayerCenter()
    {
        GameObject PlayerPoint = GameObject.FindGameObjectWithTag(SpawnPointPlayerTag);

        GameObject Player = Instantiate(PrefabPlayer);
        Player.transform.position = PlayerPoint.transform.position;
        Player.transform.rotation = PlayerPoint.transform.rotation;
    }



    public void SpawnBulletInvisibility()
    {
        if (GameManager.instance.level == 4 || GameManager.instance.level == 9 || GameManager.instance.level == 14 || GameManager.instance.level == 19 || GameManager.instance.level == 24 || GameManager.instance.level == 29 || GameManager.instance.level == 34 || GameManager.instance.level == 39 || GameManager.instance.level == 44 || GameManager.instance.level == 49)
        {
            return;
        }

        GameObject BulletInvisibilityPoint = GameObject.FindGameObjectWithTag(SpawnPointBulletInvisibilityTag);

        GameObject temp = Instantiate(PrefabBulletInvisibility);
        temp.transform.position = BulletInvisibilityPoint.transform.position;
        temp.transform.rotation = BulletInvisibilityPoint.transform.rotation;
    }

    public void SpawnBulletXray()
    {
        if (GameManager.instance.level == 4 || GameManager.instance.level == 9 || GameManager.instance.level == 14 || GameManager.instance.level == 19 || GameManager.instance.level == 24 || GameManager.instance.level == 29 || GameManager.instance.level == 34 || GameManager.instance.level == 39 || GameManager.instance.level == 44 || GameManager.instance.level == 49)
        {
            return;
        }

        GameObject BulletXrayPoint = GameObject.FindGameObjectWithTag(SpawnPointBulletXrayTag);

        GameObject temp = Instantiate(PrefabBulletXray);
        temp.transform.position = BulletXrayPoint.transform.position;
        temp.transform.rotation = BulletXrayPoint.transform.rotation;
    }


    public void DestroySpawnCenter01()
    {
        ChangeBossSeekGreen00.SetActive(true);
        ChangeBossSeekRed01.SetActive(true);
        ChangeBossSeekRed00.SetActive(false);
        ChangeBossSeekGreen01.SetActive(false);
        if (Player01!= null)
        {
            Destroy(Player01);
        }    

    }    
    public void SpawnPlayerCenter01()
    {
        ChangeBossSeekGreen00.SetActive(false);
        ChangeBossSeekRed01.SetActive(false);
        ChangeBossSeekRed00.SetActive(true);
        ChangeBossSeekGreen01.SetActive(true);

        if (GameManager.instance.level == 4 || GameManager.instance.level == 9 || GameManager.instance.level == 14 || GameManager.instance.level == 19 || GameManager.instance.level == 24 || GameManager.instance.level == 29 || GameManager.instance.level == 34 || GameManager.instance.level == 39 || GameManager.instance.level == 44 || GameManager.instance.level == 49)
        {
            return;
        }    
        if(Player01 ==null)
        {
            GameObject PlayerPoint01 = GameObject.FindGameObjectWithTag(SpawnPointPlayer01Tag);
            Player01 = Instantiate(PrefabPlayer01);
            Player01.transform.position = PlayerPoint01.transform.position;
            Player01.transform.rotation = PlayerPoint01.transform.rotation;
        } 
        else
        {
            return;
        }    


    }

    public void SpawnClock()
    {
        GameObject[] ClockPoint = GameObject.FindGameObjectsWithTag(SpawnPointClockTag);

        foreach (GameObject item in ClockPoint)
        {
            int random = Random.Range(0, PrefabsClock.Count);
            if (alwaySpawn)
            {
                GameObject pts = Instantiate(PrefabsClock[random],transform);
                pts.transform.position = item.transform.position;
                pts.transform.rotation = item.transform.rotation;
            }
            else
            {
                int spawnOrNot = Random.Range(0, 2);
                if (spawnOrNot == 0)
                {
                    GameObject ptss = Instantiate(PrefabsClock[random]);
                    ptss.transform.position = item.transform.position;
                }
            }
        }
    }


    public void SpawnSpeed()
    {
        GameObject[] SpeedPoint = GameObject.FindGameObjectsWithTag(SpawnPointSpeedTag);

        foreach (GameObject item in SpeedPoint)
        {
            int random = Random.Range(0, PrefabSpeed.Count);
            if (alwaySpawn)
            {
                GameObject pts = Instantiate(PrefabSpeed[random]);
                pts.transform.position = item.transform.position;
                pts.transform.rotation = item.transform.rotation;
            }
            else
            {
                int spawnOrNot = Random.Range(0, 2);
                if (spawnOrNot == 0)
                {
                    GameObject ptss = Instantiate(PrefabSpeed[random]);
                    ptss.transform.position = item.transform.position;
                }
            }
        }
    }

    public void MethobCheckHide00()
    {
        CheckHide00 = true;
    }
        
    public void ConvertHide()
    {
        ScoreManager.instance.coinVisibleGUI = 0;
        if (GameManager.instance.level == 4 || GameManager.instance.level == 9 || GameManager.instance.level == 14 || GameManager.instance.level == 19 || GameManager.instance.level == 24 || GameManager.instance.level == 29 || GameManager.instance.level == 34 || GameManager.instance.level == 39 || GameManager.instance.level == 44 || GameManager.instance.level == 49)
        {
            return;
        }

        ChangeStatic = true;
        Debug.Log("ChangeStatic =" + ChangeStatic);
        GameObject[] fine = GameObject.FindGameObjectsWithTag("AIHide");//tìm đến vị trí AIHide

        GameObject PlayerSeek = GameObject.FindGameObjectWithTag("PlayerSeek");//ẩn nhân vật playerseek
        PlayerSeek.SetActive(false);

        GameObject PlayerPoint01 = GameObject.FindGameObjectWithTag(SpawnPointPlayerTag);//hiển thị nhân vật AI tìm
        GameObject Player = Instantiate(PrefabPlayer01);
        Player.transform.position = PlayerPoint01.transform.position;
        Player.transform.rotation = PlayerPoint01.transform.rotation;

        GameObject playerHide = Instantiate(PrefabPlayerHide);//spawn nhân vật playerHide
        for (int i = 0; i < fine.Length; i++)
        {
            fine[3].SetActive(false);
            
            playerHide.transform.position = fine[3].transform.position;
            playerHide.transform.rotation = fine[3].transform.rotation;
        }
    }    

    public void SpawnAIAround()
    {
        foreach (GameObject item in AIPoint)
        {
            int random = Random.Range(0, PrefabsAI.Count);
            if (alwaySpawn)
            {
                GameObject pts = Instantiate(PrefabsAI[random]);
                pts.transform.position = item.transform.position;
                pts.transform.rotation = item.transform.rotation; 
            }
            else
            {
                int spawnOrNot = Random.Range(0, 2);
                if (spawnOrNot == 0)
                {
                    GameObject ptss = Instantiate(PrefabsAI[random]);
                    ptss.transform.position = item.transform.position;
                }
            }
        }

        for (int i = 0; i < AIPoint.Length; i++)
        {
            if(AIPoint[0].gameObject)
            {
                count = 1;
            }    
        }
    }
    public void SpawnPaints()
    {
        GameObject[] PaintPoint = GameObject.FindGameObjectsWithTag(SpawnPointPaintsTag);

        foreach (GameObject item in PaintPoint)
        {
            int random = Random.Range(0, PrefabsPaints.Count);
            if (alwaySpawn)
            {
                GameObject pts = Instantiate(PrefabsPaints[random]);
                pts.transform.position = item.transform.position;
                pts.transform.rotation = item.transform.rotation;
            }
            else
            {
                int spawnOrNot = Random.Range(0, 2);
                if (spawnOrNot == 0)
                {
                    GameObject ptss = Instantiate(PrefabsPaints[random]);
                    ptss.transform.position = item.transform.position;
                }
            }
        }
    }

    IEnumerator timeScale()
    {
        int random = Random.Range(0, 15);
        yield return new WaitForSeconds(random);
        SpawnClock();
        SpawnSpeed();

    } 
    
 
}


     
