﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;
using UI.ThreeDimensional;

public class UIController : MonoBehaviour
{
    public static UIController instance;
    public Camera cam;
    [SerializeField] GameObject canvasMainUI;
    [SerializeField] GameObject InSideGameUI;
    [SerializeField] GameObject CountHideArrestedUI;
    [SerializeField] GameObject WinDialogUI;
    [SerializeField] GameObject LoseDialogUI;


    [SerializeField] GameObject[] defaultUIKey;
    [SerializeField] GameObject[] IAPShopKey;
    [SerializeField] GameObject[] ChestDialogKey;
    [SerializeField] GameObject[] SettingKey;
    public RectTransform keypanel;

    public RectTransform SkinInProgress;


    public GameObject DirectionalLight;
    public GameObject[] Daily;

    public RectTransform ChestDialog;
    public RectTransform EndGamePopup;

    public List<GameObject> UiHideArrested;
    public bool isPlayGame = false;
    public bool zoomCam = false;
    [SerializeField] Text CoinText;
    public bool isWin;
    public bool isLose;

    public bool isClaim;

    public string TimeToday;
    public int day;

    public string DailyCheck;
    float intensity;
    public int keyIndex;

    Color DefaultColor;
    public GameObject ImageDailyObj;
    
    public Image FatWorm;

    #region HealthBar
    float HealthPercent;
    public float MaxHealth = 100f;
    public Image FrontHealthBar;
    #endregion

    private void Awake()
    {

        //GetComponent<UIObject3DImage>().sprite = FatWorm.sprite;


        // ImageDailyObj.SetActive(false);
        SaveColor(DefaultColor);
        updateKeyVisible();
        DailyCheck = "DailyCheck";
        instance = this;
        cam.depth = 1;
        keyIndex = Prefs.KEY;
        TimeToday = "166554" + DateTime.Today;
        for (int i = 0; i < Daily.Length; i++)
        {
            if (i < Prefs.DAY)
            {
                Daily[i].SetActive(true);
            }
        }

        if (Daily[0].activeInHierarchy == true)
        {
            day = Prefs.DAY;
        }
        else
        {
            day = 0;
        }
        Debug.Log("day"+day);
    }

    private void Start()
    {
        HealthPercent = Prefs.PERCENT;

        if (TimeToday != Prefs.TIMETODAY)
        {
            ImageDailyObj.SetActive(false);
        }
        else
        {
            ImageDailyObj.SetActive(true);
        }    
    }

    private void Update()
    {
        if (Prefs.LEVEL>=50 && WinDialogUI.activeInHierarchy==true)
        {
            EndGamePopup.DOScale(1, 2f).SetEase(Ease.OutBack);
        }    
   
        updateKeyVisible();

        if(Prefs.KEY>=3)
        {
            ChestDialog.DOAnchorPos(new Vector2(0, 0), 0.25f);
        }

        if (DirectionalLight.GetComponent<Light>().intensity > 0 && PlayerController.checkBulletXray == true)
        {
            intensity = DirectionalLight.GetComponent<Light>().intensity;
            intensity -= intensity / 8 * 0.09f;
            DirectionalLight.GetComponent<Light>().intensity = intensity;
        }else
        {
            DirectionalLight.GetComponent<Light>().intensity = 1;
        }


        for (int i = 0; i < UiHideArrested.Count; i++)
        {
            if (PatrolAgent.HideArrested >= 1)
            {

                UiHideArrested[0].SetActive(true);
            }
            else
            {
                UiHideArrested[0].SetActive(false);
            }

            if (PatrolAgent.HideArrested >= 2)
            {
                UiHideArrested[1].SetActive(true);
            }
            else
            {
                UiHideArrested[1].SetActive(false);
            }

            if (PatrolAgent.HideArrested >= 3)
            {

                UiHideArrested[2].SetActive(true);
            }
            else
            {
                UiHideArrested[2].SetActive(false);
            }

            if (PatrolAgent.HideArrested >= 4)
            {

                UiHideArrested[3].SetActive(true);
            }
            else
            {
                UiHideArrested[3].SetActive(false);
            }

            if (PatrolAgent.HideArrested == 5)
            {
                UiHideArrested[4].SetActive(true);
            }
            else
            {
                UiHideArrested[4].SetActive(false);
            }
        }
        //hiển thị coin ăn được của từng level chơi ở win
    }

    
    public void UnableCountHideArrested()
    {
        CountHideArrestedUI.SetActive(false);
    }    


    public void UpdateHealthSkinPercent()
    {
        float fillF = FrontHealthBar.fillAmount;
        Debug.Log("HealthPercent"+HealthPercent);
        Takepercent(10f);
        if(fillF<MaxHealth)
        {
            FrontHealthBar.fillAmount = Mathf.Lerp(fillF, HealthPercent / MaxHealth,Time.deltaTime* 100f);
        }
      

    }

    public void isSupperWinGame()
    {
        EndGamePopup.DOScale(0, 0.3f).SetEase(Ease.InBack);
        SceneManager.LoadScene(0);
    }    

    public void Takepercent(float healAmount)
    {
       if(HealthPercent<=MaxHealth)
        {
            HealthPercent += healAmount;
        }
        Prefs.PERCENT = HealthPercent;
        Debug.Log("percent" + Prefs.PERCENT);
    }

    public void CheckDaily()
    {
        ImageDailyObj.SetActive(true);
        for (int i = 0; i < Daily.Length; i++)
        {
            if (TimeToday != Prefs.TIMETODAY)
            {

                Daily[day].SetActive(true);
                Prefs.TIMETODAY = TimeToday;
                Debug.Log("TIMETODAY"+Prefs.TIMETODAY);
                day += 1;
                Prefs.DAY = day;
                if (Daily[0].activeInHierarchy == true && DailyCheck != Prefs.DAILY_CLICK)
                {
                    ScoreManager.instance.AddCoinDaily(150);
                    Prefs.DAILY_CLICK = DailyCheck;
                    return;
                }
                DailyCheck = "0k2";
                if (Daily[2].activeInHierarchy == true && DailyCheck != Prefs.DAILY_CLICK)
                {
                    ScoreManager.instance.AddCoinDaily(300);
                    Prefs.DAILY_CLICK = DailyCheck;
                    return;
                }

                DailyCheck = "0k2";
                if (Daily[3].activeInHierarchy == true && DailyCheck != Prefs.DAILY_CLICK)
                {
                    ScoreManager.instance.AddCoinDaily(500);
                    Prefs.DAILY_CLICK = DailyCheck;
                    return;
                }
            }    

        }


    }



    public void NothankChest()
    {
        ChestDialog.DOAnchorPos(new Vector2(0, 2649), 0.25f).SetEase(Ease.InOutCubic);
        Prefs.KEY = 0;
        
    }


    
    public void Cam()
    {
        isPlayGame = true;
        cam.depth = -1;
        zoomCam = true;
        canvasMainUI.SetActive(false);
        InSideGameUI.SetActive(true);
    }

    public void CloseSkinInProgress()
    {
        Prefs.SKINLOADINPROGRESS++;
        Debug.Log("Prefs.SKINLOADINPROGRESS =" + Prefs.SKINLOADINPROGRESS);
        ProfileShopUI.instance.skinLoadInProgress();
        SkinInProgress.DOScale(0, 0.3f).SetEase(Ease.InBack);
    }
        

    public void EndTimeWin()
    {
        if (HealthPercent > 85 && HealthPercent<100)
        {
            SkinInProgress.DOScale(1, 0.3f).SetEase(Ease.OutBack);

        }
            
        if (HealthPercent >= MaxHealth)
        {

            Debug.Log("DONE PERCENT");
            HealthPercent = 0;

            Debug.Log("DONE PERCENT" + HealthPercent);
            
        }

        StartCoroutine(timeBreakPercent());
        isPlayGame = false;
        isWin = true;
        InSideGameUI.SetActive(false);
        WinDialogUI.SetActive(true);
        AudioController.instance.WinSound();
        StartCoroutine(IEOpenChest());
    }

    IEnumerator timeBreakPercent()
    {
        yield return new WaitForSeconds(1f);
        UpdateHealthSkinPercent();
    }

    public void EndTimeLose()
    {
        isPlayGame = false;
        isLose = true;
        InSideGameUI.SetActive(false);
        LoseDialogUI.SetActive(true);
        AudioController.instance.LoseSound();
    }

    public void CoinTextt(bool yes)
    {
        CoinText.GetComponent<Animator>().enabled = yes;
    }

    public void SaveColor(Color df)
    {
        for (int i = 0; i < defaultUIKey.Length; i++)
        {
            df = defaultUIKey[i].GetComponent<RawImage>().color;
        }

        for (int i = 0; i < IAPShopKey.Length; i++)
        {
            df = defaultUIKey[i].GetComponent<RawImage>().color;
        }

        for (int i = 0; i < ChestDialogKey.Length; i++)
        {
            df = defaultUIKey[i].GetComponent<RawImage>().color;
        }
    }
    public void updateKeyVisible()
    {
        for (int i = 0; i < defaultUIKey.Length; i++)
        {
            if (i < Prefs.KEY)
            {
                defaultUIKey[i].GetComponent<RawImage>().color = Color.black;
            }

            if(Prefs.KEY<=0)
            {
                defaultUIKey[i].GetComponent<RawImage>().color = DefaultColor;
            }
        }

        for (int i = 0; i < IAPShopKey.Length; i++)
        {
            if (i < Prefs.KEY)
            {
                IAPShopKey[i].GetComponent<RawImage>().color = Color.black;
            }

            if (Prefs.KEY <= 0)
            {
                IAPShopKey[i].GetComponent<RawImage>().color = DefaultColor;
            }
        }

        for (int i = 0; i < ChestDialogKey.Length; i++)
        {
            if (i < Prefs.KEY)
            {
                ChestDialogKey[i].GetComponent<RawImage>().color = Color.black;
            }

            if (Prefs.KEY <= 0)
            {
                ChestDialogKey[i].GetComponent<RawImage>().color = DefaultColor;
            }
        }


        for (int i = 0; i < SettingKey.Length; i++)
        {
            if (i < Prefs.KEY)
            {
                SettingKey[i].GetComponent<RawImage>().color = Color.black;
            }

            if (Prefs.KEY <= 0)
            {
                SettingKey[i].GetComponent<RawImage>().color = DefaultColor;
            }
        }
    }

    private IEnumerator IEOpenChest()
    {

        yield return new WaitForSeconds(0.25f);

        int r = GameManager.instance.CustomRandomPersenForChest(new int[] { 20, 65, 10, 15 });
        Debug.Log("index mangpersen" + r);

        switch (r)
        {
            case 0:
                keyIndex += 1;
                Prefs.KEY = keyIndex;
                Debug.Log("key :" + Prefs.KEY);
                yield return new WaitForSeconds(0.5f);
                keypanel.DOScale(1, 0.3f).SetEase(Ease.OutBack);
                yield return new WaitForSeconds(2f);
                keypanel.DOScale(0, 0.3f).SetEase(Ease.InBack);
                break;
            case 1:

                break;
            case 2:
                break;
            case 3:
                break;
        }

        PlayerPrefs.Save();
    }


}
