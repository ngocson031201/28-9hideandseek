﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoADSScript : MonoBehaviour
{
    public void RemoveAds()
    {
        Prefs.FULLADS += 1;
        Prefs.BANNERADS += 1;
        ManagerAds.ins.HideBanner();
        ManagerAds.ins.RemoveInterstitialAds();
    }

    public void RemoveAdsPremium()
    {
        Prefs.FULLADS +=1;
        Prefs.BANNERADS += 1;
        Prefs.REWARDADS += 1;
        ManagerAds.ins.HideBanner();
        ManagerAds.ins.RemoveInterstitialAds();
        ManagerAds.ins.RemoveRewardAds();
    }



}
